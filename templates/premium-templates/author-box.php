<div class="card mb-4 mt-4">
  <div class="card-body">
  	<div class="container-fluid">
	  	<div class="row">

		  		<?php 
	  				$picture = esc_attr(get_option('profile_picture'));
					$firstName = esc_attr(get_option('first_name'));
					$lastName = esc_attr(get_option('last_name'));
					$fullName = $firstName . ' ' . $lastName;
					$description = esc_attr(get_option('user_description'));

					$twitter_icon = esc_attr( get_option( 'twitter_handler' ) );
					$facebook_icon = esc_attr( get_option( 'facebook_handler' ) );
					$gplus_icon = esc_attr( get_option( 'gplus_handler' ) );
					$linkedin_icon = esc_attr(get_option('linkedin_handler') );
					$instagram_icon = esc_attr(get_option('instagram_handler'));
				?>


				<!-- Author Avatar and desc box  -->
				<div class="author-avatar col-12 col-sm-12 col-md-12 col-lg-2 d-flex justify-content-center align-items-center">
		  			<img src="<?php echo $picture; ?>" width="96"; height="96"; alt="profile-pic" class="rounded-circle">
		  		</div>

				<div class="author-meta col-12 col-sm-12 col-md-12 col-lg-10 ">
				    <h4 class="author-title d-flex justify-content-md-center justify-content-lg-start align-items-center"><?php echo $fullName; ?></h4> 
				    <span id="author-des d-flex justify-content-md-center justify-content-lg-start align-items-center" class="author-desc"><?php echo $description; ?></span>
				</div>

				<!-- Author Desc social meta box -->
				<div class="cleanblogger-author-social-icons col-8 col-sm-8 coml-md-10 offset-2 offset-sm-2 offset-md-2 m-auto">

					<?php if( !empty( $twitter_icon ) ): ?>
						<a href="https://twitter.com/<?php echo $twitter_icon; ?>" target="_blank"<i class="fa fa-twitter-square fa-lg" aria-hidden="true"></i></a>
					<?php endif; ?>

					<?php if( !empty( $facebook_icon ) ): ?>
						<a href="https://facebook.com/<?php echo $facebook_icon; ?>" target="_blank"><i class="fa fa-facebook-square fa-lg" aria-hidden="true"></i></a>
					<?php endif; ?>

					<?php if( !empty( $gplus_icon ) ): ?>
						<a href="https://plus.google.com/u/0/+<?php echo $gplus_icon; ?>" target="_blank"><i class="fa fa-google-plus-square fa-lg" aria-hidden="true"></i></a>
					<?php endif; ?>

					<?php if( !empty( $linkedin_icon ) ): ?>
						<a href="https://facebook.com/<?php echo $linkedin_icon; ?>" target="_blank"><i class="fa fa-linkedin-square fa-lg" aria-hidden="true"></i></a>
					<?php endif; ?>

					<?php if( !empty( $instagram_icon ) ): ?>
						<a href="https://facebook.com/<?php echo $instagram_icon; ?>" target="_blank"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a>
					<?php endif; ?>	

				</div> <!-- .social-icons  -->
		 </div>
	</div>
  </div>
</div>