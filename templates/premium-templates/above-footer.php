
<div class="container m-auto">
	<div class="row">
		<div class="col-sm-12">
	
			<div class="col-sm-12 m-auto">
				<h5 id="comment" class="mb-4 mt-0 comments-section text-center">
					<?php 

						printf(
							esc_html(_nx( 'One comment on %2$s', '%1$s comments on %2$s', 
								get_comments_number(), 'comments title', 'cleanblogger' )), 
							number_format_i18n( get_comments_number()), '<span class="comment-list-title">&ldquo;' . get_the_title() . '&rdquo;</span>');

					 ?>
				</h5>

				<!-- comments -->
				<?php cleanblogger_comment(); ?>
				
			</div>
			
			<h4 class="d-flex justify-content-center mt-6 mb-4 text-center"><strong>More from <?php bloginfo('name') ?></strong></h4>

			<!-- related posts -->
			<div class="card-deck d-flex flex-md-column flex-lg-row justify-content-center mt-lg-6">

				<?php
					$post_id = get_the_ID();
					$cat_ids = '';
					$categories = get_the_category( $post_id );

					if(!empty($categories) && !is_wp_error($categories)):
						foreach ($categories as $category):
							$cat_ids .= $category->name . ', ';

						endforeach;
					endif;

					$current_post_type = get_post_type($post_id);
					$query_args = array( 

						'category_name' => $cat_ids,
						'post_type'      => 'post',
						'post_not_in'  => array($post_id),
						'posts_per_page'  => '3'
						

					 );

					$related_cats_post = new WP_Query( $query_args );
					
					if($related_cats_post->have_posts()):
						 while($related_cats_post->have_posts()): $related_cats_post->the_post(); ?>
						 	<div class="card mt-md-4">

								<a rel="external" href="<? the_permalink()?>"><?php the_post_thumbnail('cleanblogger_related_post_img',  array('class' => 'img-fluid', )); ?></a>

							    <div class="card-body">
								    <h5 class="card-title">
								    	<a href="<?php the_permalink(); ?>">
								    		<?php echo wp_trim_words( get_the_title(), 9, '...' ); ?>
								    	</a>
								    </h5>
									<span id="related-post-excerpt" class=""><?php the_excerpt(); ?></span >
							    </div><!-- .card-body -->

							    <div class="card-footer d-flex justify-content-between">
							    			  		<?php 
											  			$picture = esc_attr(get_option('profile_picture'));
												  		$firstName = esc_attr(get_option('first_name'));
														$lastName = esc_attr(get_option('last_name'));
														$fullName = $firstName . ' ' . $lastName;
													?>
							    	<div class="d-flex justify-content-start align-items-center">

							    		<img src="<?php print $picture; ?>" width="32"; height="32"; alt="pic" class="rounded-circle">
										<div class="ml-2 d-flex flex-column">
											<span class="author-desc-meta"><?php echo $fullName; ?></span>
											<span class="author-desc-meta"><?php the_time( '' ); ?></span>
										</div>
							    	</div>

							    	<div id="author-desc-wrap" class="d-flex justify-content-start align-items-center flex-column">
							    		<span id="author-descmeta" class="author-desc-meta"><?php echo cleanblogger_reading_time(); ?></span>
								    	<small class="text-muted author-desc-meta">
									    	<?php echo cleanblogger_related_post_meta(); ?>	
								    	</small>
								    </div>
							    </div><!-- .card-footer -->

							 </div><!-- .card -->
						<?php 

							endwhile;
							
						 	// Restore original Post Data
						 	wp_reset_postdata();

						 	endif;

					 	?>

			</div><!-- end deck -->

		</div>
	</div>
</div>
