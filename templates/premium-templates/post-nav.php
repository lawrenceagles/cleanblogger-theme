<div class="clearfix mt-4 mb-4">
	<div class="row">
		<div class="col-sm-12 d-flex flex-md-row justify-content-md-between">

		<?php 

			$prev = get_previous_post_link( '<div id="cleanblogger-btn" class="post-link-nav btn btn-primary float-md-left"> %link</div>', '&laquo; Prev Post', false, '', 'category' );
			$next = get_next_post_link( '<div id="cleanblogger-btn" class="post-link-nav text-right btn btn-primary float-md-right"> %link</div>', 'Next Post &raquo;', false, '', 'category' );
		?>

		<div class=""><?php echo $prev ?></div>
			
		<div class=""><?php echo $next ?></div>		 

	</div>
 </div><!-- clearfix -->