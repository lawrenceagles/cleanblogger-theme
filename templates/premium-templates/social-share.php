<div class="card mb-4 mt-4">
  <div class="card-body">
  	<h5 class="card-title text-center">Like what you read? Share with a friend</h5>
		<?php 
			$title = get_the_title();
			$permalink = get_the_permalink();
			$twitterHandler = (get_option('twitter_handler') ? '&amp;via='. esc_attr(get_option('twitter_handler')): '');

			$facebook   = 'https://www.facebook.com/sharer/sharer.php?u=' .$permalink;
			$twitter    = 'https://twitter.com/intent/tweet?text=' .$title. '&amp;url=' .$permalink. $twitterHandler;
			$googleplus = 'https://plus.google.com/share?url='.$permalink;

		?>
		
			
		<div class="d-flex justify-content-center">

		 	<a href="<?php echo $facebook ?>" rel="nofollow" target="_blank">
				<span class="fa-stack fa-lg">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
				</span>
			</a>

			<a href="<?php echo $twitter ?>" rel="nofollow" target="_blank">
				<span class="fa-stack fa-lg">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
				</span>
			</a>

			<a href="<?php echo $googleplus ?>" rel="nofollow" target="_blank">
				<span class="fa-stack fa-lg">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
				</span>
			</a>
		</div>
		   		
  </div><!-- end card-body -->
</div><!-- end card -->