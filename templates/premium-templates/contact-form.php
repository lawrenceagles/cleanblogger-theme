<form name="sentMessage" id="cleanblogger-contact-form" method="post" data-url="<?php echo admin_url( 'admin-ajax.php', 'admin' ); ?>" novalidate>

  <div class="control-group">
    <div class="form-group floating-label-form-group controls">
      <label>Name</label>
      <input type="text" class="form-control" placeholder="Name" id="name" required data-validation-required-message="Please enter your name.">
      <p class="help-block text-danger"></p>
    </div>
  </div>

  <div class="control-group">
    <div class="form-group floating-label-form-group controls">
      <label>Email Address</label>
      <input type="email" class="form-control" placeholder="Email Address" id="email" required data-validation-required-message="Please enter your email address.">
      <p class="help-block text-danger"></p>
    </div>
  </div>

  <div class="control-group confirmEmail-group">
    <div class="form-group floating-label-form-group controls">
      <label>Confirm Email Address</label>
      <input type="email" class="form-control" placeholder="Confirm Email Address" id="confirmEmail">
    </div>
  </div>

  <div class="control-group">
    <div class="form-group col-xs-12 floating-label-form-group controls">
      <label>Phone Number</label>
      <input type="tel" class="form-control" placeholder="Phone Number" id="phone" required data-validation-required-message="Please enter your phone number.">
      <p class="help-block text-danger"></p>
    </div>
  </div>


  <div class="control-group d-none">
    <div class="form-group floating-label-form-group controls">
      <label>Mobile Number</label>
      <input type="tel" class="form-control d-none" placeholder="Mobile Number" id="telephone">
    </div>
  </div>


  <div class="control-group url-group">
    <div class="form-group floating-label-form-group controls">
      <label>Website</label>
      <input type="url" class="form-control" placeholder="Website" id="url">
    </div>
  </div>

  <div class="control-group">
    <div class="form-group floating-label-form-group controls">
      <label>Message</label>
      <textarea rows="5" class="form-control" placeholder="Message" id="message" required data-validation-required-message="Please enter a message."></textarea>
      <p class="help-block text-danger"></p>
    </div>
  </div>

  <br>
  <div id="success"></div>
  <div class="form-group">
    <button type="submit" class="btn btn-primary" id="sendMessageButton"><i id="cform-icon" class="fa fa-paper-plane-o fa-lg fa-fw"></i>
<span class="sr-only">Sending...</span>Send</button>
  </div>
  
</form>