<div class="cleanblogger-sidebar sidebar-closed">

	<div class="row">
		<div class="container">

			<div style="height: 100vh;" class="cleanblogger-sidebar-container position-relative d-block w-100 ">

				<a style="z-index: 99;" class="sidebar-close position-absolute p-2">
					<i class="fa fa-times fa-lg js-toggleSidebar" aria-hidden="true"></i>
				</a>

				<div class="sidebar-scroll w-100 h-100">

					<?php get_sidebar(); ?>

				</div><!-- .sidebar-scroll -->

			</div><!-- .cleanblogger-sidebar-container -->

		</div>
	</div>

</div><!-- .cleanblogger-sidebar -->