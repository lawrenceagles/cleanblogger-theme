<!-- Page Header -->
<header class="masthead" style="background-image: url('<?php echo get_the_post_thumbnail_url() ?>')">
  <div class="overlay"></div>

  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">

        <div class="page-heading">

          <?php the_title( '<h1 class="text-center">', '</h1>' ); ?>
          <hr class="small">

          <?php 
              $subtitle = get_post_meta(get_the_ID(), '_subtitle_form_field_key', true);
              if(isset($subtitle)):
                echo '<span class="entry-subtitle subheading text-center">'. $subtitle .'</span>';
              endif;
          ?>

        </div><!-- .post-heading -->

      </div><!-- .mx-auto -->
    </div><!-- .row -->

  </div><!-- .container -->
</header><!-- header -->