<!-- Page Header -->
<?php 
  $display = false; 
  // Get only image url
  $params = array(
    'term_id' => null,
    'size' => 'full'
  );
  $category_image_src = category_image_src( $params , $display );
 ?>
<header class="masthead" style="background-image: url('<?php echo $category_image_src; ?>')">
  <div class="overlay"></div>

  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">

        <div class="post-heading">

           <h1 class="text-center">Posts From <?php  single_cat_title(); ?> Category</h1>
           <hr class="small">
           
        </div><!-- .post-heading -->

      </div><!-- .mx-auto -->
    </div><!-- .row -->

  </div><!-- .container -->
</header><!-- header -->