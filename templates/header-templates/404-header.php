<!-- 404 Page Header -->
<header class="masthead" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/404-error.jpg')">
  <div class="overlay"></div>

  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">

        <div class="page-heading">

         <h1 class="page-title"><?php esc_html_e( 'Oops 404 Error! That page can&rsquo;t be found!', 'cleanblog' ); ?></h1>

          <span class="entry-subtitle subheading text-center"><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'cleanblog' ); ?></span>

        </div><!-- .post-heading -->

      </div><!-- .mx-auto -->
    </div><!-- .row -->

  </div><!-- .container -->
</header><!-- header -->