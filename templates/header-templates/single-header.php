<!-- Page Header -->
<header class="masthead" style="background-image: url('<?php echo get_the_post_thumbnail_url() ?>')">
  <div class="overlay"></div>

  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">

        <div class="post-heading">

          <?php the_title( '<h1>', '</h1>' ); ?>
                  
          <?php 
              $subtitle = get_post_meta(get_the_ID(), '_subtitle_form_field_key', true);
              if(isset($subtitle)):
                echo '<h3 class="entry-subtitle subheading">'. $subtitle .'</h3>';
              endif;
          ?>



          <span class="entry-meta meta">
            <?php echo cleanblogger_posted_meta() ?>
          </span><!-- .entry-meta -->

        </div><!-- .post-heading -->

      </div><!-- .mx-auto -->
    </div><!-- .row -->

  </div><!-- .container -->
</header><!-- header -->