<!-- Page Header -->
<header class="masthead" style="background-image: url('<?php header_image(); ?>')">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="site-heading">
          <h1 class="text-center"><?php bloginfo( 'name' ); ?></h1>
            <hr class="small">
          <span class="subheading text-center"><?php bloginfo( 'description' ); ?></span>
        </div>
      </div>
    </div>
  </div>
</header>