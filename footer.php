<?php
/**
 * ===========================================
 * 	Cleanblogger footer template
 * 	
 * 	@package cleanblogger
 * ============================================
 */

$twitterHandler = esc_attr(get_option('twitter_handler'));
$facebookHandler = esc_attr(get_option('facebook_handler'));
$gplusHandler = esc_attr(get_option('gplus_handler'));

$twitterProfile = esc_html( 'https://twitter.com/' );
$facebookProfile = esc_html( 'https://facebook.com/' );
$googlePlus      = esc_html( 'https://plus.google.com/' );
?>



	<div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto footer">
          	
              <div class="d-flex justify-content-center">
                  <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
                    <div id="footer-social-follow">
                      <?php dynamic_sidebar( 'sidebar-1' ); ?>
                    </div>
                  <?php endif; ?>
              </div>

            <div class="copyright text-muted text-center">Copyright &copy; <?php bloginfo( 'name' ); ?> <?php the_date('Y'); ?></div>
          </div><!-- .m-auto -->
        </div><!-- .row -->
     </div> <!-- .container -->
     
  <?php wp_footer(); ?>
	</body><!-- body -->
</html>
