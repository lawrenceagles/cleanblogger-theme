<?php

/**
 * ===========================================
 * 	Cleanblogger functions and definitions for the custom admin are
 * 	
 * 	@package cleanblogger
 * ============================================
 */


/**
 * ===========================================
 * Functions to implement a custom admin area
 * ============================================
 */

require get_template_directory() . '/inc/admin-functions.php';

/**
 * ===========================================
 * Functions to get theme-support.php file
 * ============================================
 */

require get_template_directory() . '/inc/theme-support.php';


/**
 * ===========================================
 * Functions to enqueue files
 * ============================================
 */

require get_template_directory() . '/inc/enqueue.php';


/**
 * ===========================================
 * Functions to get remove Wordpress Version
 * ============================================
 */

require get_template_directory() . '/inc/mu-plugins/example.php';


/**
 * ===========================================
 * Functions to get custom-post-types.php file
 * ============================================
 */

require get_template_directory() . '/inc/custom-post-types.php';

/**
 * ===========================================
 * Functions to get remove Wordpress Version
 * ============================================
 */

require get_template_directory() . '/inc/security.php';


/**
 * ===========================================
 * Functions to get contact_me.php file
 * ============================================
 */

require get_template_directory() . '/inc/ajax.php';


/**
 * ===========================================
 * Functions to get contact_me.php file
 * ============================================
 */

require get_template_directory() . '/inc/customizer.php';

/**
 * ===========================================
 * Functions create widget areas file
 * ============================================
 */

require get_template_directory() . '/inc/widget-areas.php';

/**
 * ===========================================
 * Functions create widget areas file
 * ============================================
 */

require get_template_directory() . '/inc/widgets.php';


