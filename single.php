<?php 
/**
 * ===========================================
 * 	Cleanblogger functions to activate theme support
 * 	
 * 	@package cleanblogger
 * ============================================
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<div class="container">
			<?php

				if(have_posts()):
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content', 'single');

					endwhile; // End of the loop.
				endif;
				
			?>
		</div> <!-- .container -->
	
		<!-- above footer -->
		<footer class="entry-above-footer mb-4" style="">
			<div class="col-sm-12 m-auto">
				<?php echo cleanblogger_cats_related_post(); ?>
			</div>
		</footer>

	</main><!-- #main -->
</div><!-- #primary -->


<?php get_footer(); ?>


