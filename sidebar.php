<?php
/** 
 * ============================================
 * Functions call to dynamic sidebar 
 * ============================================
 */

if ( ! is_active_sidebar( 'cleanblogger-sidebar' ) ) {
	return;
}

?>

<aside id="secondary" class="widget-area" role="complementary">
	
	<?php dynamic_sidebar( 'cleanblogger-sidebar' ); ?>
	
</aside>