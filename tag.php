<?php 
/**
 * ===========================================
 * 	Cleanblogger for displaying the tags page
 * 	
 * 	@package cleanblogger
 * ============================================
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<div class="container">
			<?php

				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', get_post_format());

				endwhile; // End of the loop.
				
			?>
		</div> <!-- .container -->

	</main><!-- #main -->
</div><!-- #primary -->


<?php get_footer(); ?>


