<?php
/**
 * ===========================================
 * 	Cleanblogger hearder template
 * 	
 * 	@package cleanblogger
 * ============================================
 */

?>

<!DOCTYPE html>
<html lang="<?php language_attributes() ?>">

	<head>
	
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">

		<?php if(is_singular() && pings_open(get_queried_object())): ?>
			<link rel="pingback" href="<?php bloginfo('pingback_url') ?>">
		<?php endif; ?>

		<?php wp_head(); ?>

		 <script id='pixel-script-poptin' src='https://cdn.popt.in/pixel.js?id=d315f0dabc367' async='true'></script> 
	</head><!-- head -->
	
	<?php $args_class = array('d-flex', 'justify-content-center'); ?>
	<body class="<?php body_class($args_class); ?>">

		<div class="sidebar-overlay w-100 h-100"></div>

		<?php get_template_part( 'templates/premium-templates/dynamic', 'sidebar' ); ?>

		<!-- Navigation -->
	    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
	    	<div class="container">
				  	<?php if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ): ?>
						<?php the_custom_logo(); ?> 
					<?php else:?>

					<a class="navbar-brand" href="<?php echo esc_url(home_url()); ?>">
						<?php bloginfo('title'); ?> 
					<?php endif; ?> 
					</a>
				  	

			  <button class="navbar-toggler mobile-menu" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			  	Menu
			    <i class="fa fa-caret-down" aria-hidden="true"></i>
			  </button>

			   <!-- the walker nav menu -->
				<?php

					wp_nav_menu( array(
						'theme_location'    => 'main_menu',
						'depth'             => 0,
						'container'         => 'div',
						'container_class'   => 'collapse navbar-collapse',
						'container_id'      => 'navbarNav',
						'menu_class'        => 'navbar-nav ml-auto',
						'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
						'walker'            => new WP_Bootstrap_Navwalker(),
					) );
					
				?>

			    <!-- take this menu into the dynamic sidebar on mobile -->
				<a class="sidebar-open position-absolute">
					<i class="fa fa-bars fa-lg fa-2x js-toggleSidebar" aria-hidden="true"></i>
				</a>
			</div>
		</nav>

		<?php 
			if(is_home() || is_front_page()):
				get_template_part('templates/header-templates/home', 'header');
			elseif(is_single()):
				get_template_part('templates/header-templates/single', 'header');
			elseif(is_tag()):
				get_template_part('templates/header-templates/tag', 'header');
			elseif(is_page()):
				get_template_part('templates/header-templates/page', 'header');
			elseif(is_category()):
				get_template_part('templates/header-templates/category', 'header');
			elseif(is_404()):
				get_template_part('templates/header-templates/404', 'header');
			endif;
		?>



