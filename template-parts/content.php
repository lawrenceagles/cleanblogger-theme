<?php
/**
 * ===========================================
 * 	Template part for displaying posts
 * 	
 * 	@package cleanblogger
 * ============================================
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">

		<!-- Main Content -->
	    <div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-10 mx-auto">
					<div class="post-preview">
							
						<a href="<?php the_permalink( $post ); ?>">
							<?php the_title( '<h2 class="entry-title post-title">', '</h2>', true ); ?>

							<?php 
				              $subtitle = get_post_meta(get_the_ID(), '_subtitle_form_field_key', true);
				              if(isset($subtitle)):
				                echo '<h3 class="entry-subtitle post-subtitle">'. $subtitle .'</h3>';
				              endif;
					        ?>
						</a>

						<div class="entry-meta post-meta">
							<?php echo cleanblogger_posted_meta() ?>
						</div><!-- .entry-meta -->
				 		<hr>
					</div><!-- .post-preview -->
				</div><!-- .col-lg-8 -->
			</div><!-- .row -->
	    </div><!-- .container -->

	</div>	    
</article><!-- #post-<?php the_ID(); ?> -->
