<?php
/**
 * ===========================================
 * 	Template part for displaying posts
 * 	
 * 	@package cleanblogger
 * ============================================
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">

		<!-- Main Content -->
	    <div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-10 mx-auto">
					
					<?php the_content(); ?>

				</div>
			</div><!-- .row -->
	    </div><!-- .container -->
	    <hr>
	    
	</div>    
</article><!-- #post-<?php the_ID(); ?> -->
