<?php
/**
 * ===========================================
 * 	Template part for displaying posts
 * 	
 * 	@package cleanblogger
 * ============================================
 */

?>

<?php $args_class = array('d-flex', 'justify-content-center'); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class($args_class); ?>>

	<!-- Main Content -->
    <div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-10 mx-auto">
				
		        <div class="entry-content">
					<?php the_content(); ?>
				</div><!-- entry-content -->
				<hr>

				<!-- Comment Link -->
				<div class="entry-comment mt-4 mb-4">
					<?php echo cleanblogger_posted_footer() ?>
				</div><!-- .entry-comment -->

				<!-- Sharethis -->						
				<?php echo cleanblogger_sharethis(); ?>
				
				<!-- author box -->	
				<?php echo cleanblogger_author_box(); ?>

				<!-- Post Nav -->	
				<?php echo cleanblogger_single_post_nav(); ?>
			</div><!-- .col-lg-8 -->
		</div><!-- .row -->
    </div><!-- .container -->
	    
</article><!-- #post-<?php the_ID(); ?> -->
