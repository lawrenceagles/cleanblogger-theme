<?php 
/**
 * ===========================================
 * 	Cleanblogger functions to activate theme support
 * 	
 * 	@package cleanblogger
 * ============================================
 */


if( post_password_required() ):
	return;
endif;

?>

<div id="comments" class="comments-area col-sm-8 m-auto">
	
	<?php if(have_comments()): ?>
		
		<ol class="comment-list">
			<?php 

				$args = array (

					'walker' 				=> null,
					'max-depth' 			=> '',
					'style'					=> 'ol',
					'avatar_size'			=> 64,
					'callback'				=> null,
					'end_callback'			=> null,
					'type'					=> 'all',
					'reply_text'			=> 'Reply',
					'page'					=> '',
					'per_page'				=> '',
					'reverse_top_level' 	=> '',
					'reverse_children'		=> '',
					'format'				=> 'html5',
					'short_ping'			=> false,
					'echo'					=> true

				);

				wp_list_comments( array( $args ) );

			 ?>
		</ol>

		<?php cleanblogger_comment_nav(); ?>

		<?php	if(!comments_open() && get_commments_number()): ?>
	

		<p class="no-comments"><?php esc_html_e( 'Comments are closed', 'cleanblogger' ); ?></p>
			
		<?php endif; ?>
	<?php endif; ?>

	<?php 
			$commenter = wp_get_current_commenter();
			$req = get_option( 'require_name_email' );
			$aria_req = ( $req ? " aria-required='true'" : '' );

			$fields = array(
			
			'author' =>
				'<div class="form-group"><label for="author">' . esc_html__( 'Name', 'domainreference' ) . '</label> <span class="required">*</span> <input id="author" name="author" type="text" class="form-control" value="' . esc_attr( $commenter['comment_author'] ) . '" '.$aria_req.' /></div>',

				
			'email' =>
				'<div class="form-group"><label for="email">' . esc_html__( 'Email', 'domainreference' ) . '</label> <span class="required">*</span><input id="email" name="email" class="form-control" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" '.$aria_req.' /></div>',
				
			'url' =>
				'<div id="website" class="form-group last-field"><label for="url">' . esc_html__( 'Website', 'domainreference' ) . '</label><input id="url" name="url" class="form-control" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" /></div>'
				
		);

		$args = array (
			'class_submit' 	=> 'btn btn-block btn-md btn-primary btn-comment',
			'label_submit'	=>	esc_html__('Submit Comment', 'domainreference'),
			'label_submit' => esc_html__( 'Submit Comment', 'domainreference' ),
			'comment_field' =>
				'<div id="commentform" class="form-group"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label> <span class="required">*</span><textarea id="comment" class="form-control" name="comment" rows="3" required="required"></textarea></p>',
			'fields' => apply_filters( 'comment_form_default_fields', $fields ),
			'title_reply' => 'Leave a comment'
		);

		comment_form($args);
	?>
	
</div>