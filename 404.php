<?php 
/**
 * ===========================================
 * 	Cleanblogger 404 Error Template
 * 	
 * 	@package cleanblogger
 * ============================================
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<div class="container">
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="entry-content">

					<!-- Main Content -->
				    <div class="container">
						<div class="row">
							<div class="col-lg-8 col-md-10 mx-auto">
								
								<div class="d-flex justify-content-center"><a href="<?php echo home_url(); ?>" role="button" class="btn btn-warning btn-lg">Return to home</a></div>

							</div><!-- .col-lg-8 -->
						</div><!-- .row -->
				    </div><!-- .container -->

				</div>	    
			</article><!-- #post-<?php the_ID(); ?> -->

		</div> <!-- .container -->

	</main><!-- #main -->
</div><!-- #primary -->


<?php get_footer(); ?>


