<?php 
/**
 * ===========================================
 * 	Cleanblogger functions to activate theme support
 *
 * Template Name: Home Template
 * 	
 * 	@package cleanblogger
 * ============================================
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<div class="container infinite-loop-posts">
			
			<?php

					while ( have_posts() ) : the_post();

						//$class = 'reveal';
						//set_query_var( 'post-class', $class );

						get_template_part( 'template-parts/content', 'get_post_format()');

					endwhile; // End of the loop.
				
			?>


			<!-- Pager -->
			<div class="clearfix col-sm-8 m-auto">
				<?php if ( get_previous_posts_link() ) : ?>
					<div id="cleanblogger-btn" class="btn btn-primary float-md-left mb-2 btn-post-nav">
						<?php previous_posts_link( '&laquo; Previous Page' ); ?>
					</div>
				<?php  endif; ?>
				
				<?php if ( get_next_posts_link() ) : ?>
					<div id="cleanblogger-btn" class="btn btn-primary float-md-right mb-2 btn-post-nav">
						<?php  next_posts_link( 'Next Page &raquo;' ); ?>
					</div>
				<?php endif; ?>
			</div>

		</div> <!-- .container -->

	</main><!-- #main -->
</div><!-- #primary -->


<?php get_footer(); ?>


