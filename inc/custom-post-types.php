<?php
/**
 * ===========================================
 * 	Cleanblogger functions and definitions for 
 * 	CPT generation and activations
 * 	
 * 	@package cleanblogger
 * ============================================
 */



/** 
 * ============================================
 * Functions activate custom contact 
 * ============================================
 */
$contact = get_option('contact_form');
if(@$contact == 1):

	// creates the contact for cpt only if it is checked by user
	add_action( 'init', 'cleanblogger_contact_form_cpt' );

	//manage my custom post types [contact-form-cpt] columns
	add_filter('manage_contact-form-cpt_posts_columns', 'contact_form_set_columns_callback');

	// manage contact-form-cpt custom columns
	add_action( 'manage_contact-form-cpt_posts_custom_column', 'contact_form_set_custom_columns_callback', 10, 2 );

	// create contact form meta box
	add_action ('add_meta_boxes', 'contact_form_meta_box');

	add_action('save_post', 'save_contact_form_email_value');

endif;


/** 
 * ============================================
 * Functions create Contact Form CPT
 * ============================================
 */

function cleanblogger_contact_form_cpt() {

	$labels = array(
		'name'				=> 'Messages',
		'singular_name'		=> 'Message',
		'menu_name'			=> 'Messages',
		'name_admin_bar'	=> 'Message'
	);

	$args = array(
		'labels'			=> $labels,
		'show_ui'			=> true,
		'show_in_menu'		=> true,
		'capability_type'	=> 'post',
		'hierarchical'		=> false,
		'menu_position'		=> 26,
		'menu_icon'			=> 'dashicons-email-alt',
		'supports'			=> array('title', 'editor', 'author')
	);

	register_post_type('contact-form-cpt', $args);
}



/** 
 * ============================================
 * Functions edit contact-form-cpt columns
 * ============================================
 */
function contact_form_set_columns_callback($columns) {

	$newColumns = array();

	$newColumns['title'] = 'Full Name';
	$newColumns['message'] = 'Message';
	$newColumns['email'] = 'Sender Email';
	$newColumns['date'] = 'Date Sent';


	return $newColumns;
}



/** 
 * ============================================
 * Functions mange contact-form-cpt custom columns
 * ============================================
 */
function contact_form_set_custom_columns_callback($column, $post_id) { 
	
	switch($column) {

		case 'message':
			echo get_the_excerpt();
			break;
		
		case 'email':
		$userEmail = get_post_meta( $post_id, '_contact_form_email_key', true ); 
			echo '<a href="mailto: '. $userEmail .'">'. $userEmail .'</a>';
			break;
	}
}




/** 
 * ============================================
 * Functions add contact form meta box column
 * ============================================
 */

function contact_form_meta_box() {
	add_meta_box( 'contact-form-meta-box', __( 'User Email', 'cleanblogger' ), 'contact_form_meta_box_callback', 'contact-form-cpt', 'side' );
}


/** 
 * ============================================
 * Functions create contact form meta box 
 * column centent
 * ============================================
 */

function contact_form_meta_box_callback($post){

	wp_nonce_field( 'save_contact_form_email_data', 'meta_box_contact_form_nonce');
	$value = get_post_meta( $post->ID, '_contact_form_email_key', true ); 

	echo '<label for="contact_form_email_field">User Email Address: </label>';

	echo '<input type="email" id="contact_form_email_field" name="contact_form_email_field" value="' . esc_attr( $value ) . '" size="25" />';
}



function save_contact_form_email_value ($post_id) {


	if(! current_user_can( 'edit_posts')):
		return;
	endif;

	if(! isset( $_POST['meta_box_contact_form_nonce'] )):
		return;
	endif;

	if(! wp_verify_nonce( $_POST['meta_box_contact_form_nonce'], 'save_contact_form_email_value' )):
		return;
	endif;

	if(! isset( $_POST['contact_form_email_field'] )):
		return;
	endif;

	if(define('DOING_AUTOSAVE') && DOING_AUTOSAVE):
		return;
	endif;

	$emailData = sanitize_email( $_POST['contact_form_email_field']);
	update_post_meta( $post_id, '_contact_form_email_key', $emailData );


}


/** 
 * ============================================
 * Functions definitions add subtitle supports
 * ============================================
 */

/** 
 * ============================================
 * Functions add subtitle form meta box column
 * ============================================
 */

function subtitle_form_meta_box() {
	add_meta_box( 'subtitle-form-meta-box', __( 'Subtitle', 'cleanblogger' ), 'subtitle_form_meta_box_callback', 'post', 'advanced', 'high' );
}

// create contact form meta box
add_action ('add_meta_boxes', 'subtitle_form_meta_box');

// Move all "advanced" metaboxes above the default editor
add_action('edit_form_after_title', function() {
    global $post, $wp_meta_boxes;
    do_meta_boxes(get_current_screen(), 'advanced', $post);
    unset($wp_meta_boxes[get_post_type($post)]['advanced']);
});


/** 
 * ============================================
 * Functions create subtitle form meta box 
 * column centent
 * ============================================
 */

function subtitle_form_meta_box_callback(){

    global $post; ## global post object

    wp_nonce_field( plugin_basename( __FILE__ ), 'meta_box_subtitle_form_nonce' ); ## Create nonce

    $subtitle = get_post_meta($post->ID, '_subtitle_form_field_key', true); ## Get the subtitle

    ?>
	
	<div>
	    <input type="text" name="_subtitle_form_field_key" id="_subtitle_form_field_key" class="widefat" value="<?php if(isset($subtitle)) { echo $subtitle; } ?>" placeholder="Enter subtitle" />
	</div>

    <?php
}



function save_subtitle_form_value ($post_id, $post) {


	if(! current_user_can( 'edit_post', $post->ID)):
		return $post->ID;
	endif;

	if(! isset( $_POST['meta_box_subtitle_form_nonce'] )):
		return $post->ID;
	endif;

	if(! wp_verify_nonce( $_POST['meta_box_subtitle_form_nonce'], plugin_basename(__FILE__))):
		return $post->ID;
	endif;

	if(define('DOING_AUTOSAVE') && DOING_AUTOSAVE):
		return $post->ID;
	endif;

	$subtitleData =  sanitize_text_field( $_POST['_subtitle_form_field_key'] );

	if ($subtitleData):
		update_post_meta($post_id, '_subtitle_form_field_key', $_POST['_subtitle_form_field_key']);
	else:
		update_post_meta($post_id, '_subtitle_form_field_key', '');
	endif;

	return false;

}

add_action('save_post', 'save_subtitle_form_value', 1, 2);
