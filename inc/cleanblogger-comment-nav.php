<nav id="comment-nav-top" class="comment-navigation" role="navigation">
	<h5 class="comment-navigation"><?php esc_html_e('Comment navigation', 'cleanblogger'); ?></h5>
	<div class="row">
		<div class="col-sm-12 d-flex justify-content-between align-content-between">
			<div class="">
				<div id="comment-nav-link" class="post-link-nav">
					<!-- fontawesome icon here -->
					<?php previous_comments_link(esc_html__( '&laquo; Older', 'cleanblogger' )); ?>
				</div>
			</div>

			<div class="">
				<div id="comment-nav-link" class="post-link-nav">
					<!-- fontawesome icon here -->
					<?php next_comments_link(esc_html__( 'Newer &raquo;', 'cleanblogger' )); ?>
				</div>
			</div>
		</div>
	</div>
</nav>