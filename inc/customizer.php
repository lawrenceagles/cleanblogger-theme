<?php
/**
 * cleanblogger Theme Customizer
 *
 * @package cleanblogger
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function cleanblogger_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'cleanblogger_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'cleanblogger_customize_partial_blogdescription',
		) );
	}

	//Color scheme functions definitions
	$wp_customize->add_section( 'textcolors', array(
		'title' => 'Cleanblogger Color Scheme',
	) );

	$textColors[] = array(
		'slug' 					=> 'primary_color_scheme',
		'default'				=> '#212529',
		'label'					=>  esc_html__( 'Primary Color', 'cleanblogger' ),
		'transport'				=> 'postMessage', 
		'sanitize_callback'		=> 'sanitize_hex_color'
	);

	$textColors[] = array(
		'slug' 					=> 'secondary_color_scheme',
		'default'				=> '#868e96',
		'label'					=> esc_html__( 'Secondary Color', 'cleanblogger' ),
		'transport'				=> 'postMessage', 
		'sanitize_callback'		=> 'sanitize_hex_color'
	);

	$textColors[] = array(
		'slug' 					=> 'above_footer_bg',
		'default'				=> '#efefef',
		'label'					=> esc_html__( 'Above Footer Background', 'cleanblogger' ),
		'transport'				=> 'postMessage', 
		'sanitize_callback'		=> 'sanitize_hex_color'
	);

	$textColors[] = array(
		'slug' 					=> 'accent_color_scheme',
		'default'				=> '#0085A1',
		'label'					=> esc_html__( 'Accent Color', 'cleanblogger' ),
		'transport'				=> 'postMessage', 
		'sanitize_callback'		=> 'sanitize_hex_color'
	);

	$textColors[] = array(
		'slug' 					=> 'tooltip_bg_color',
		'default'				=> '#17a2b8',
		'label'					=> esc_html__( 'Tooltip Color', 'cleanblogger' ),
		'transport'				=> 'postMessage', 
		'sanitize_callback'		=> 'sanitize_hex_color'
	);

	$textColors[] = array(
		'slug' 					=> 'bootstrap_btn_color_scheme',
		'default'				=> '#17a2b8 !important',
		'label'					=> esc_html__( 'Buttons Background Color', 'cleanblogger' ),
		'transport'				=> 'postMessage', 
		'sanitize_callback'		=> 'sanitize_hex_color'
	);

	$textColors[] = array(
		'slug' 					=> 'bootstrap_btn_hover_color_scheme',
		'default'				=> '#efefe7 !important',
		'label'					=> esc_html__( 'Buttons Accent Background Color', 'cleanblogger' ),
		'transport'				=> 'postMessage', 
		'sanitize_callback'		=> 'sanitize_hex_color'
	);

	// add setings and control for each color.
	foreach ($textColors as $textColor):
		//settings
		$wp_customize->add_setting($textColor['slug'], array(
			'default'	=> $textColor['default'],
			'type'		=> 'option',
			'capability'	=> 'edit_theme_options'
		));

		//controls
		$wp_customize->add_control(
			new WP_Customize_Color_Control (
				$wp_customize,
				$textColor['slug'], 
				array(
					'label'		=> $textColor['label'],
					'section'	=> 'textcolors',
					'settings'		=> $textColor['slug']
				)
			)
		);

	endforeach;


	function cleanblogger_color_scheme_css() {
		// define main colors here
		$primary_color_scheme = get_option('primary_color_scheme');
		$secondary_color_scheme = get_option('secondary_color_scheme');
		$above_footer_bg = get_option('above_footer_bg');
		$accent_color_scheme = get_option('accent_color_scheme');
		$bootstrap_btn_color_scheme = get_option('bootstrap_btn_color_scheme');
		$bootstrap_btn_hover_color_scheme = get_option('bootstrap_btn_hover_color_scheme');
		$tooltip_bg_color = get_option('tooltip_bg_color');
		
		/**
		 * =================================================
		 * customizer css
		 * =================================================
		 */

		?>

		<style type="text/css">
			/* color scheme */

			/* Primary color scope */
			body, p, .post-preview > a {
				color: <?php echo $primary_color_scheme ?>
			}

			/* secondary color scope */
			blockquote {
				color: <?php echo $secondary_color_scheme; ?>
			}

			/* accent aka link:hover, active color scope */

			.post-preview > a:hover, 
			.post-preview > a:focus {
				color: <?php echo $accent_color_scheme . ' !important'; ?>
			}



			/* bootstrap primary bg color */
			div#cleanblogger-btn, 
			button.btn-primary,
			a#footer-social-icons > span > i.fa-inverse:hover {
				background-color : <?php echo $bootstrap_btn_color_scheme; ?>;
				border-color : <?php echo $bootstrap_btn_color_scheme; ?>;
			}

			/* footer social icons */
			a#footer-social-icons > .social-icons:hover {
				background-color : <?php echo $bootstrap_btn_color_scheme; ?>;
				border-color : <?php echo $bootstrap_btn_color_scheme; ?>;
				color:  #FFFFFF;
				border-radius: 100% !important;
			}

			  div#cleanblogger-btn:hover,
			  div#cleanblogger-btn:focus,
			  div#cleanblogger-btn:active {
				background-color : <?php echo $bootstrap_btn_hover_color_scheme . ' !important'; ?>;
				border-color : <?php echo $bootstrap_btn_hover_color_scheme . ' !important'; ?>;
			  }

			  button.btn-primary:hover,
			  button.btn-primary:focus,
			  button.btn-primary:active {
				background-color : <?php echo $bootstrap_btn_hover_color_scheme . ' !important'; ?>;
				border-color : <?php echo $bootstrap_btn_hover_color_scheme . ' !important'; ?>;
			  }

			/* above footer section background color */
			footer.entry-above-footer {
			  background-color: <?php echo $above_footer_bg . ' !important'; ?>;
			}

			/* tooltip text background color */
			p > span.cleanblogger-popover {
			    background-color: <?php echo $tooltip_bg_color; ?>;
			    border-color: <?php echo $tooltip_bg_color; ?>;
			}
			

		</style>

		<?php // Re-opening the php properly
	}

	add_action('wp_head', 'cleanblogger_color_scheme_css');
}
add_action( 'customize_register', 'cleanblogger_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function cleanblogger_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function cleanblogger_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function cleanblogger_customize_preview_js() {
	wp_enqueue_script( 'cleanblogger-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'cleanblogger_customize_preview_js' );
