<?php
/**
 * ===========================================
 * 	Cleanblogger functions to activate theme support
 * 	
 * 	@package cleanblogger
 * ============================================
 */

/**
 * ============================================
 * Register widget area.
 *
 * Regiters widget areas 
 * ============================================
 */
function cleanblogger_widgets_init() {

	register_sidebar( array(
		'name'          => esc_html__( 'Cleanblogger Dynamic Sidebar', 'cleanblogger' ),
		'id'            => 'cleanblogger-sidebar',
		'description'   => esc_html__( 'Add sidebar items here.', 'cleanblogger' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Social Follow', 'cleanblogger' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'cleanblogger' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'cleanblogger_widgets_init' );
