<?php

/**
 * ===========================================
 * 	Cleanblogger enqueue function definitions
 *
 *  Enqueue Admin Scripts and styles
 * 	
 * 	@package cleanblogger
 * ============================================
 */




/** 
 * ============================================
 * Enqueue Admin Scripts And CSS
 * ============================================
 */

function cleanblogger_load_admin_scripts ($load) {

	if ('toplevel_page_sidebar_options' != $load):
		return;
	endif;

	// register new admin style
	wp_register_style( 'cleanblogger_admin', get_stylesheet_directory_uri() . '/assets/css/cleanblogger-admin.css', array(), false, 'all' );

	wp_enqueue_style( 'cleanblogger_admin');


	// enqueue wordpress media uploader functions
	wp_enqueue_media();


	// register media js
	wp_register_script( 'cleanblogger_admin-script', get_template_directory_uri() . '/assets/js/cleanblogger-admin.min.js', array( 'jquery' ), false, true );

	wp_enqueue_script( 'cleanblogger_admin-script');
}

add_action('admin_enqueue_scripts', 'cleanblogger_load_admin_scripts');




/** 
 * ============================================
 * Enqueue Frontend Scripts And CSS
 * ============================================
 */
function cleanblogger_load_scripts() {

	wp_enqueue_style( 'Gfonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800|Roboto:400,400i,700,700i|Source+Sans+Pro:400,400i,700,700i');

	wp_enqueue_style( 'Font-Awesome-cdn', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css
');

	wp_enqueue_style( 'Font-Awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css');

	wp_enqueue_style( 'bootstrap-4', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css', array(), '4.1.1', 'all' );

	// load Bootstrap locally if cdn fails
	wp_enqueue_style( 'Bootstrap-local', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css');

	wp_register_style( 'cleanblogger_css', get_stylesheet_directory_uri() . '/assets/css/cleanblogger.css', array(), false, 'all' );

	wp_enqueue_style( 'cleanblogger_css');

	wp_enqueue_script( 'popper-js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array( 'jquery' ), false, true );

	wp_enqueue_script( 'bootstrap-4-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js', array( 'jquery' ), false, true );

	wp_enqueue_script( 'jqValidation-contact-form', get_template_directory_uri() . '/assets/js/jqBootstrapValidation.min.js', array( 'jquery' ), false, true );


	wp_enqueue_script( 'cleanblogger_js', get_template_directory_uri() . '/assets/js/cleanblogger.min.js', array( 'jquery' ), false, true );

	wp_enqueue_script( 'cleanblogger_customizer_js', get_template_directory_uri() . '/assets/js/customizer.min.js', array( 'jquery' ), false, true );
}

add_action( 'wp_enqueue_scripts','cleanblogger_load_scripts' );
