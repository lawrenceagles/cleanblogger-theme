<?php
/*

@package cleanblogger theme

	========================
		WIDGET CLASS
	========================
*/

class Cleanblogger_Profile_Widget extends WP_Widget {

	//setup the widget name, description, etc...
	public function __construct() {

		$widget_ops = array(
			'classname' => 'cleanblogger-profile-widget',
			'description' => 'Cleanblogger Author Profile Box Widget',
		);
		parent::__construct( 'cleanblogger_profile', 'Cleanblogger Profile Box', $widget_ops );

	}

	//back-end display of widget
	public function form( $instance ) {
		echo '<p><strong>No options for this Widget!</strong><br/>You can control setings of this Widget from <a href="./admin.php?page=sidebar_options">This Page</a></p>';
	}

	//front-end display of widget
	public function widget( $args, $instance ) {

		$picture = esc_attr(get_option('profile_picture'));
		$firstName = esc_attr(get_option('first_name'));
		$lastName = esc_attr(get_option('last_name'));
		$fullName = $firstName . ' ' . $lastName;
		$description = esc_attr(get_option('user_description'));

		$twitter_icon = esc_attr( get_option( 'twitter_handler' ) );
		$facebook_icon = esc_attr( get_option( 'facebook_handler' ) );
		$gplus_icon = esc_attr( get_option( 'gplus_handler' ) );
		$linkedin_icon = esc_attr(get_option('linkedin_handler') );
		$instagram_icon =esc_attr(get_option('instagram_handler'));

		echo $args['before_widget'];
		?>
		
			<section class="">
				<div class="row">

					<div class="cleanblogger-sidebar-wrap">
						<div class="cleanblogger-sidebar-content">
							<div class="container-profile-picture">

								<div id="profile-piture-preview" class="profile-piture-content" style="background-image: url(<?php print $picture; ?>);">
								</div><!-- proifile-picture-content -->

							</div><!-- container-profile-picture -->

							<h1><?php print $fullName; ?></h1>
							<h2><?php print $description ?></h2>

							<div class="cleanblogger-sidebar-social-icons">

								<?php if( !empty( $twitter_icon ) ): ?>
									<a href="https://twitter.com/<?php echo $twitter_icon; ?>" target="_blank"<i class="fa fa-twitter-square" aria-hidden="true"></i></a>
								<?php endif; ?>

								<?php if( !empty( $facebook_icon ) ): ?>
									<a href="https://facebook.com/<?php echo $facebook_icon; ?>" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
								<?php endif; ?>

								<?php if( !empty( $gplus_icon ) ): ?>
									<a href="https://plus.google.com/u/0/+<?php echo $gplus_icon; ?>" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
								<?php endif; ?>

								<?php if( !empty( $linkedin_icon ) ): ?>
									<a href="https://facebook.com/<?php echo $linkedin_icon; ?>" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
								<?php endif; ?>

								<?php if( !empty( $instagram_icon ) ): ?>
									<a href="https://facebook.com/<?php echo $instagram_icon; ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
								<?php endif; ?>

								

							</div>
						</div>
					</div>

				</div><!-- row -->
			</section><!-- end section -->
		
		<?php
		echo $args['after_widget'];
	}

}

add_action( 'widgets_init', function() {
	register_widget( 'cleanblogger_Profile_Widget' );
} );


/*
	Edit default WordPress widgets
*/

function cleanblogger_tag_cloud_font_change( $args ) {

	$args['smallest'] = 8;
	$args['largest'] = 8;

	return $args;

}
add_filter( 'widget_tag_cloud_args', 'cleanblogger_tag_cloud_font_change' );

function cleanblogger_list_categories_output_change( $links ) {

	$links = str_replace('</a> (', '</a> <span>', $links);
	$links = str_replace(')', '</span>', $links);

	return $links;

}
add_filter( 'wp_list_categories', 'cleanblogger_list_categories_output_change' );


