<?php
/**
 * ===========================================
 * 	Cleanblogger functions and definitions for the custom admin are
 * 	
 * 	@package cleanblogger
 * ============================================
 */


/**
 * ============================================
 * Function to generate dashboard menu on admin bar 
 * and dashboard calls. the cleanblogger_theme_create_sidebar_panel function
 * to generate the admin page.
 * ============================================
 */
function cleanblogger_add_admin_page () {

	// generate cleanblogger admin page
	add_menu_page( 'cleanblogger Theme Manager', 'cleanblogger', 'manage_options', 'sidebar_options', 'cleanblogger_theme_create_sidebar_panel', 'dashicon-admin-cusomizer', 110);

		// generate cleanblogger sub menu pages
		// generate the sidebar options submenu page
		add_submenu_page( 'sidebar_options', 'cleanblogger Sidebar Options', 'Sidebar Options', 'manage_options', 'sidebar_options', 'cleanblogger_theme_create_sidebar_panel' );

		// generate the theme options sub menu page
		add_submenu_page( 'sidebar_options', 'cleanblogger Theme Options', 'Theme Options', 'manage_options', 'cleanblogger_theme_options', 'cleanblogger_create_theme_options_panel' );

		// generate the contact form sub menu page
		add_submenu_page( 'sidebar_options', 'cleanblogger Contact', 'Contact Form', 'manage_options', 'cleanblogger_contact_form', 'cleanblogger_create_contact_form_panel' );

		// generate the custom css sub menu page
		add_submenu_page( 'sidebar_options', 'cleanblogger Custom CSS', 'Custom CSS', 'manage_options', 'cleanblogger_custom_css', 'cleanblogger_create_custom_css_panel' );

	// generate sub menu page contents
	add_action('admin_init', 'admin_template_create');

}

// calls cleanblogger_add_admin_page during the generation of the admin menu!
add_action('admin_menu', 'cleanblogger_add_admin_page');



/**
 * ============================================
 * Function to generate admin template content
 * ============================================
 */

function admin_template_create () {

	#Sidebar Options
	
	// creates a section in the wp_options table in the database
	register_setting( 'cleanblogger-settings-group', 'profile_picture');
	register_setting( 'cleanblogger-settings-group', 'first_name');
	register_setting( 'cleanblogger-settings-group', 'last_name');
	register_setting( 'cleanblogger-settings-group', 'user_description');
	register_setting( 'cleanblogger-settings-group', 'twitter_handler', 'cleanblogger_sanitize_twitter_handler');
	register_setting( 'cleanblogger-settings-group', 'facebook_handler');
	register_setting( 'cleanblogger-settings-group', 'gplus_handler');
	register_setting( 'cleanblogger-settings-group', 'googleplus_Url');
	register_setting( 'cleanblogger-settings-group', 'github_handler');
	register_setting( 'cleanblogger-settings-group', 'linkedin_handler');
	register_setting( 'cleanblogger-settings-group', 'instagram_handler');


	// add a subsection to the generated group
	add_settings_section( 'cleanblogger-sidebar-options', 'Sidebar Options', 'cleanblogger_sidebar_options', 'sidebar_options' );
	//add a field in the generated sub section
	add_settings_field( 'cleanblogger-profile-picture', 'Profile Picture', 'cleanblogger_create_picture_field', 'sidebar_options', 'cleanblogger-sidebar-options');

	add_settings_field( 'cleanblogger-sidebar-name', 'Full Name', 'cleanblogger_create_sidebar_field', 'sidebar_options', 'cleanblogger-sidebar-options');
	add_settings_field( 'cleanblogger-sidebar-description', 'User Description', 'cleanblogger_create_sidebar_description_field', 'sidebar_options', 'cleanblogger-sidebar-options');

	add_settings_field( 'cleanblogger-twitter-name', 'Twitter Handler', 'cleanblogger_create_sidebar_twitter_handler', 'sidebar_options', 'cleanblogger-sidebar-options');

	add_settings_field( 'cleanblogger-facebook-name', 'Facebook Handler', 'cleanblogger_create_sidebar_facebook_handler', 'sidebar_options', 'cleanblogger-sidebar-options');

	add_settings_field( 'cleanblogger-gplus-name', 'Google+ Handler', 'cleanblogger_create_sidebar_gplus_handler', 'sidebar_options', 'cleanblogger-sidebar-options');

	add_settings_field( 'cleanblogger-goolgeplus-Url', 'Google+ Author Url', 'cleanblogger_create_sidebar_googleplus_url', 'sidebar_options', 'cleanblogger-sidebar-options');

	add_settings_field( 'cleanblogger-github-name', 'Github Handler', 'cleanblogger_create_sidebar_github_handler', 'sidebar_options', 'cleanblogger-sidebar-options');

	add_settings_field( 'cleanblogger-linkedin-name', 'Linkedin Handler', 'cleanblogger_create_sidebar_linkedin_handler', 'sidebar_options', 'cleanblogger-sidebar-options');

	add_settings_field( 'cleanblogger-instagram-name', 'Instagram Handler', 'cleanblogger_create_sidebar_instagram_handler', 'sidebar_options', 'cleanblogger-sidebar-options');


	#Theme Options

	// Create theme-options section in the wp_options database table
	register_setting( 'cleanblogger-theme-support', 'social_share' );
	register_setting( 'cleanblogger-theme-support', 'author_box' );
	register_setting( 'cleanblogger-theme-support', 'related_posts' );
	register_setting( 'cleanblogger-theme-support', 'posts_nav' );
	register_setting( 'cleanblogger-theme-support', 'custom_header' );
	register_setting( 'cleanblogger-theme-support', 'custom_background' );

	// create theme-options page section
	add_settings_section( 'cleanblogger-theme-options', 'Theme Options', 'theme_options', 'cleanblogger_theme_options' );

	//create post formats fields
	add_settings_field( 'related-posts', 'Related Posts', 'cleanblogger_related_posts_callback', 'cleanblogger_theme_options', 'cleanblogger-theme-options');

	//create post formats fields
	add_settings_field( 'author-box', 'Author Box', 'cleanblogger_author_box_callback', 'cleanblogger_theme_options', 'cleanblogger-theme-options');

	//create post formats fields
	add_settings_field( 'social-share', 'Social Share', 'cleanblogger_social_share_callback', 'cleanblogger_theme_options', 'cleanblogger-theme-options');

	//create post posts nav
	add_settings_field( 'posts-nav', 'Posts Navigation', 'cleanblogger_posts_nav_callback', 'cleanblogger_theme_options', 'cleanblogger-theme-options');

	//create custom header fields
	add_settings_field( 'custom-header', 'Custom Header', 'cleanblogger_custom_header_callback', 'cleanblogger_theme_options', 'cleanblogger-theme-options');

	//create custom background fields
	add_settings_field( 'custom-background', 'Custom Background', 'cleanblogger_custom_background_callback', 'cleanblogger_theme_options', 'cleanblogger-theme-options');


	#Contact form 
	
	// Create contact form section in the wp_options database table
	register_setting( 'cleanblogger-contact-section', 'contact_form');

	add_settings_section( 'cleanblogger-contact-form', 'Contact Form', 'cleanblogger_contact_form_callback', 'cleanblogger_contact_form' );

	add_settings_field( 'contact-form', 'Contact Form', 'cleanblogger_contact_form_field_callback', 'cleanblogger_contact_form', 'cleanblogger-contact-form' );

}



/** 
 * ============================================
 * Begin Sidebar options functions
 * ============================================
 */

/**
 * ============================================
 * Function to generate the subsection content fields
 * ============================================
 */
function cleanblogger_create_picture_field() {
	// create variables to save the input and populate the value field
	$picture = esc_attr(get_option('profile_picture'));

	if(empty($picture)):

		//print the form templates fields
		echo '<input type="button" value="Upload Picture" id="cleanblogger-profile-picture" class="button button-secondary" /><input type="hidden" value="" name="profile_picture" id="profile_picture" />';
	else:
	
		//print the form templates fields
		echo '<input type="button" value="Replace Profile Picture" name="profile_picture" id="cleanblogger-profile-picture" class="button button-secondary" /> 

		 <input type="button" value="Remove" name="profile_picture" id="cleanblogger-remove-profile-picture" class="button button-secondary" />' ;
	endif;
}



/**
 * ============================================
 * Function to generate the subsection content fields
 * ============================================
 */
function cleanblogger_create_sidebar_field () {
	// create variables to save the input and populate the value field
	$firstName = esc_attr(get_option('first_name'));
	$lastName = esc_attr(get_option('last_name'));

	//print the form templates fields
	echo '<input type="text" value="'.$firstName.'" name="first_name" placeholder="First Name" /> <input type="text" value="'.$lastName.'" name="last_name" placeholder="Last Name" />';
}


/**
 * ============================================
 * Function to generate the twitter handler field
 * ============================================
 */
function cleanblogger_create_sidebar_description_field () {
	// create variables to save the input and populate the value field
	$description = esc_attr(get_option('user_description'));

	//print the form templates fieldsdescription
	echo '<input type="text" value="'.$description.'" name="user_description" placeholder="Description" /><p>write something smart</p>';
}


/**
 * ============================================
 * Function to generate the twitter handler field
 * ============================================
 */
function cleanblogger_create_sidebar_twitter_handler () {
	// create variables to save the input and populate the value field
	$twitterHandler = esc_attr(get_option('twitter_handler'));

	//print the form templates fields
	echo '<input type="text" value="'.$twitterHandler.'" name="twitter_handler" placeholder="Twitter Handler" /><p><em>Input your twitter username without the @</em></p>';
}


/**
 * ============================================
 * Function to generate the facebook handler field
 * ============================================
 */
function cleanblogger_create_sidebar_facebook_handler () {
	// create variables to save the input and populate the value field
	$facebookHandler = esc_attr(get_option('facebook_handler'));

	//print the form templates fields
	echo '<input type="text" value="'.$facebookHandler.'" name="facebook_handler" placeholder="Facebook Handler" />';
}


/**
 * ============================================
 * Function to generate the gplus handler field
 * ============================================
 */
function cleanblogger_create_sidebar_gplus_handler () {
	// create variables to save the input and populate the value field
	$gplusHandler = esc_attr(get_option('gplus_handler'));

	//print the form templates fields
	echo '<input type="text" value="'.$gplusHandler.'" name="gplus_handler" placeholder="Google+ Handler" />';
}

/**
 * ============================================
 * Function to generate the gplus handler field
 * ============================================
 */
function cleanblogger_create_sidebar_googleplus_url () {
	// create variables to save the input and populate the value field
	$googleplusUrl = esc_attr(get_option('googleplus_Url'));

	//print the form templates fields
	echo '<input type="text" value="'.$googleplusUrl.'" name="googleplus_Url" placeholder="Google+ Author Url" /><p><em>Input your google profile full Url</em></p>';
}

/**
 * ============================================
 * Function to generate the github handler field
 * ============================================
 */
function cleanblogger_create_sidebar_github_handler () {
	// create variables to save the input and populate the value field
	$githubHandler = esc_attr(get_option('github_handler'));

	//print the form templates fields
	echo '<input type="text" value="'.$githubHandler.'" name="github_handler" placeholder="Github Handler" />';
}



/**
 * ============================================
 * Function to generate the linkedin handler field
 * ============================================
 */
function cleanblogger_create_sidebar_linkedin_handler () {
	// create variables to save the input and populate the value field
	$linkedinHandler = esc_attr(get_option('linkedin_handler'));

	//print the form templates fields
	echo '<input type="text" value="'.$linkedinHandler.'" name="linkedin_handler" placeholder="Linkedin Handler" />';
}


/**
 * ============================================
 * Function to generate the instagram handler field
 * ============================================
 */
function cleanblogger_create_sidebar_instagram_handler () {
	// create variables to save the input and populate the value field
	$instagramHandler = esc_attr(get_option('instagram_handler'));

	//print the form templates fields
	echo '<input type="text" value="'.$instagramHandler.'" name="instagram_handler" placeholder="Instagram Handler" />';
}



/**
 * ============================================
 * Function to generate the sub section content
 * ============================================
 */

function cleanblogger_sidebar_options () {
	echo 'Enter information for your Author Box and Author Profile';
}


/**
 * ============================================
 * Function to sanitize the twitter handler user input
 * ============================================
 */
function cleanblogger_sanitize_twitter_handler ($input) {

	$output = sanitize_text_field($input);
	$output = str_replace('@', '', $output);
	return $output;
}


/**
 * ============================================
 * Function to generate admin page sidebar options
 * ============================================
 */
function cleanblogger_theme_create_sidebar_panel () {
	//generation of the admin page.
	require_once (get_template_directory() . '/inc/admin-templates/cleanblogger-sidebar-template.php');
}



/** 
 * ============================================
 * Begin Theme options functions
 * ============================================
 */

/**
 * ============================================
 * Function to generate admin page theme options
 * ============================================
 */
function cleanblogger_create_theme_options_panel () {
	//generation of the theme options page.
	require_once (get_template_directory() . '/inc/admin-templates/cleanblogger-theme-options-template.php');
}



/**
 * ============================================
 * Function to generate admin page theme options
 * ============================================
 */
function cleanblogger_theme_options_callback($input) {
	return $input;
}


/**
 * ============================================
 * The Theme options Function for post formats
 * ============================================
 */
function theme_options () {
	echo '<h2>Activate or Deactivate The Theme Optios of Your Choice</h2>';
}


/**
 * ============================================
 * Callback function for social_share
 * ============================================
 */
function cleanblogger_social_share_callback () {

	$options = get_option('social_share');
	$checked = (@$options == 1 ? 'checked':'');
	echo '<label><input type="checkbox" name="social_share" id="social_share" value="1" '.$checked.' />Activate Built-in Social Share Feature</label><br>';
}


/**
 * ============================================
 * Callback function for author_box
 * ============================================
 */
function cleanblogger_author_box_callback () {

	$options = get_option('author_box');
	$checked = (@$options == 1 ? 'checked':'');
	echo '<label><input type="checkbox" name="author_box" id="author_box" value="1" '.$checked.' />Activate Built-in Author Box Feature</label><br>';
}


/**
 * ============================================
 * Callback function for posts_nav
 * ============================================
 */
function cleanblogger_posts_nav_callback () {

	$options = get_option('posts_nav');
	$checked = (@$options == 1 ? 'checked':'');
	echo '<label><input type="checkbox" name="posts_nav" id="posts_nav" value="1" '.$checked.' />Activate Built-in Posts Navigation Feature</label><br>';
}


/**
 * ============================================
 * Callback function for related_posts
 * ============================================
 */
function cleanblogger_related_posts_callback () {

	$options = get_option('related_posts');
	$checked = (@$options == 1 ? 'checked':'');
	echo '<label><input type="checkbox" name="related_posts" id="related_posts" value="1" '.$checked.' />Activate Built-in Related Post Feature</label><br>';
}


/**
 * ============================================
 * Callback function for custom_header
 * ============================================
 */
function cleanblogger_custom_header_callback () {

	$options = get_option('custom_header');
	$checked = (@$options == 1 ? 'checked':'');
	echo '<label><input type="checkbox" name="custom_header" id="custom_header" value="1" '.$checked.' />Activate Custom Header Feature</label><br>';

}



/**
 * ============================================
 * Callback function for custom_background
 * ============================================
 */
function cleanblogger_custom_background_callback () {

	$options = get_option('custom_background');
	$checked = (@$options == 1 ? 'checked':'');
	echo '<label><input type="checkbox" name="custom_background" id="custom_background" value="1" '.$checked.' />Activate Custom Background Feature</label><br>';

}




/** 
 * ============================================
 * Begin Custom Contact Form Functions
 * ============================================
 */


/**
 * ============================================
 * Function to generate contact form template
 * ============================================
 */
function cleanblogger_create_contact_form_panel () {
	//generation of the theme options page.
	require_once (get_template_directory() . '/inc/admin-templates/cleanblogger-contact-form-template.php');
}



/**
 * ============================================
 * Function to generate user information
 * ============================================
 */
function cleanblogger_contact_form_callback() {
	echo "Activete or Deactivate The Built-in Contact Form";
}



/**
 * ============================================
 * Function to generate section field
 * ============================================
 */
function cleanblogger_contact_form_field_callback() {

	$contact = get_option('contact_form');
	$checked = (@$contact == 1 ? 'checked':'');
	echo '<input type="checkbox" name="contact_form" value="1 id="contact-form" '.$checked.' />';
}




/** 
 * ============================================
 * Begin Custom css functions
 * ============================================
 */

/**
 * ============================================
 * Function to generate admin page Custom css options
 * ============================================
 */
function cleanblogger_create_custom_css_panel () {
	//generation of the theme options page.
	require_once (get_template_directory() . '/inc/admin-templates/cleanblogger-custom-css.php');
}

/**
 * ============================================
 * Function to generate admin page custom css
 * ============================================
 */
function cleanblogger_theme_create_custom_css_page () {
	//generation of the admin page.
	
}
