<?php 
/**
 * ===========================================
 * 	Cleanblogger functions definitions for ajax calls
 * 	
 * 	@package cleanblogger
 * ============================================
 */

add_action( 'wp_ajax_nopriv_cleanblogger_save_contactForm_msg', 'cleanblogger_save_contactForm_msg' );
add_action( 'wp_ajax_cleanblogger_save_contactForm_msg', 'cleanblogger_save_contactForm_msg' );

function cleanblogger_save_contactForm_msg() {

	$name = wp_strip_all_tags($_POST['name']);
	$email = wp_strip_all_tags($_POST['email']);
	$phone = wp_strip_all_tags($_POST['phone']);
	$confirmEmail = wp_strip_all_tags($_POST['confirmEmail']);
	$url = wp_strip_all_tags($_POST['url']);
	$telephone = wp_strip_all_tags($_POST['telephone']);
	$message = wp_strip_all_tags($_POST['message']);


	$args = array (
		'post_title'	=> $name,
		'post_content'	=> $message,
		'post_status'	=> 'publish',
		'post_author'	=> 1,
		'post_type'		=> 'contact-form-cpt',
		'meta_input' 	=>	array (
			'_contact_form_email_key'	=> $email
		)
    );

		if(empty($confirmEmail) && empty($url )):
			$post_ID = wp_insert_post( $args, false );
			echo $post_ID;

			// checks if wp_insert_post was successful!
			if($post_ID !== 0):

				$to = get_bloginfo('admin_email');
				$site_message = bloginfo('title');
				$subject = 'New'.$site_message.'Message From - ' . $name; 
				$message = $message;
				$website = get_bloginfo('name');
				$headers[] = 'From: ' .$website. '<' .$to. '>'; // proper would not go into spam folder.
				$headers[] = 'Reply To' .$name. '<' .$email. '>';
				$headers[] = 'Content-Type: text/html: charset = UTF-8';

				wp_mail( $to, $subject, $message, $headers);
			else:
				echo 0;
			endif;

		else:
			return;
		endif;


			

	die();
}