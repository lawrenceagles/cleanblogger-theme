<?php
/**
 * ===========================================
 * 	Cleanblogger functions to activate theme support
 * 	
 * 	@package cleanblogger
 * ============================================
 */


/** 
 * ============================================
 * Functions activate author box 
 * ============================================
 */
$header = get_option('custom_header');
if(@$header == 1):
	add_theme_support('custom-header');
endif;


/** 
 * ============================================
 * Functions activate custom header 
 * ============================================
 */

$header = get_option('custom_header');
if(@$header == 1):
	add_theme_support('custom-header');
endif;



/** 
 * ============================================
 * Functions activate custom header 
 * ============================================
 */
$background = get_option('custom_background');
if(@$background == 1):
	add_theme_support('custom-background');
endif;

/** 
 * ============================================
 * Functions definition for google authorship
 * ============================================
 */

function cleanblogger_add_google_authorship () {
	$googleplusUrl = esc_attr(get_option('googleplus_Url'));
	echo '<link rel="author" href="'. $googleplusUrl .'"></link>';
}

add_action('wp_head', 'cleanblogger_add_google_authorship');

/** 
 * ============================================
 * Functions definitions add custom theme 
 * supports
 * ============================================
 */

if ( ! function_exists( 'cleanblogger_setup' ) ) :

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function cleanblogger_setup() {

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on cleanblogger, use a find and replace
		 * to change 'cleanblogger' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'cleanblogger', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'main_menu' => esc_html__( 'primary navigation', 'cleanblogger' )

		) );


		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'cleanblogger_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 40,
			'width'       => 142,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'cleanblogger_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function cleanblogger_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'cleanblogger_content_width', 640 );
}
add_action( 'after_setup_theme', 'cleanblogger_content_width', 0 );


// Activate theme support for page and product only if plugin is active
if ( in_array( 'markdown-editor/markdown-editor.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ):
	// checks if the plugin is active
	//add markdown for page 
	add_post_type_support( 'page', 'wpcom-markdown' );

	//add mardown for products
	add_post_type_support( 'product', 'wpcom-markdown' );
endif;

/**
 * ==================================
 * Flush out the transients used in 
 * cleanblogger_categorized_blog.
 * =================================
 */
function cleanblogger_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'cleanblogger_categories' );
}
add_action( 'edit_category', 'cleanblogger_category_transient_flusher' );
add_action( 'save_post',     'cleanblogger_category_transient_flusher' );

/////////////////////////////////////////
//Github Updater functions difinitions //
/////////////////////////////////////////
add_filter( 'github_updater_disable_wpcron', '__return_true' );
add_filter( 'ghu_always_fetch_update', '__return_true' );

////////////////////////////////
//Declare woocommerce support //
////////////////////////////////
//add_theme_support( 'woocommerce' );

/**
 * ========================================
 * Load WooCommerce compatibility file.
 * ========================================
 */


if ( class_exists( 'WooCommerce' ) ):
	require get_template_directory() . '/inc/woocommerce.php';
endif;


/** 
 * ============================================
 * Function add bootsrap 4 walker class navigation
 * ============================================
 */

if ( ! file_exists( get_template_directory() . '/class-wp-bootstrap-navwalker.php' ) ) {
	// file does not exist... return an error.
	return new WP_Error( 'class-wp-bootstrap-navwalker-missing', esc_html_e( 'It appears the class-wp-bootstrap-navwalker.php file may be missing.', 'wp-bootstrap-navwalker' ) );
} else {
	// file exists... require it.
    require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}




/** 
 * ============================================
 * Function for posted on meta
 * ============================================
 */

function cleanblogger_posted_meta() {

	$posted_on = human_time_diff( get_the_time('U'), current_time('timestamp') );
	$categories = get_the_category();
	$seperator = ', ';
	$output = '';
	$counter = 1;

	if(!empty($categories)):
		foreach ($categories as $category):
			if($counter > 1): $output .= $seperator; endif;
			$output .= '<a href="'. esc_url( get_category_link( $category->term_id ) ) .'" alt="'. esc_attr( 'view all posts in %s', $category->name ) .'">'. esc_html( $category->name ) .'</a>';
			$counter++;
		endforeach;
	endif;

	return '<span class="posted_on"> Posted: <a href="'. esc_url(get_permalink()) .'">'. $posted_on .' ago </a></span> <span class="posted_in">'. $output .'</span>';
}



/** 
 * ============================================
 * Function for related post meta
 * ============================================
 */

function cleanblogger_related_post_meta() {

	$posted_on = human_time_diff( get_the_time('U'), current_time('timestamp') );
	$categories = get_the_category();
	$seperator = ', ';
	$output = '';
	$counter = 1;

	if(!empty($categories)):
		foreach ($categories as $category):
			if($counter > 1): $output .= $seperator; endif;
			$output .= '<a href="'. esc_url( get_category_link( $category->term_id ) ) .'" alt="'. esc_attr( 'view all posts in %s', $category->name ) .'">'. esc_html( $category->name ) .'</a>';
			$counter++;
		endforeach;
	endif;

	return '<span class="related_posted_in">'. $output .'</span>';
}



/** 
 * ============================================
 * Function for comment counts and link to comments
 * ============================================
 */

function cleanblogger_posted_footer() {
	// gets the number of comments
	$comments_number = get_comments_number();

	// checks if comments are open (allowed or set)
	if(comments_open()):

		// prints different results depending on number of comments
		if($comments_number > 1):
			$comments = $comments_number . esc_html__(' awesome comments', 'cleanblogger');
		elseif($comments_number == 0):
			$comments = esc_html__('No Comments. Be the first to comment', 'cleanblogger');
		else:
			$comments = esc_html__('1 Comments', 'cleanblogger');
		endif;

		$comments = '<a class="comments-link" href="' . get_comments_link() . '">'. $comments .' <i></i></a>';
	else:
		$comments = esc_html__('Comments Are Closed', 'cleanblogger');
	endif;

	return '<div class="cleanblogger-post-footer-wrap">
				<div class="row">
					<div class="col-sm-12 col-md-6 d-flex justify-content-md-start justify-content-sm-center align-content-center">'
						.get_the_tag_list( '<div class="cleanblogger-post-tags-list">', ' ', '</div>').
					'</div><div class="col-sm-12 col-md-6 d-flex justify-content-md-end justify-content-sm-center align-content-center">'
					 	//.$comments.  
					 	.'<span class="comment-link-wrap">'. $comments .'</span>'.
					'</div>
				</div>
			</div>';
}


/** 
 * ============================================
 * Function for cleanblogger comments support
 * ============================================
 */
 
 function cleanblogger_comment() {

	if(comments_open()): 
		comments_template();
	endif;
 	
 }

 /** 
 * ============================================
 * Function for cleanblogger reply comments.js
 * ============================================
 */

 function cleanbogger_comment_reply() {
 	if(is_singular() && comments_open() && get_option('thread_comments')):
 		wp_enqueue_script( 'comment-reply' );
	 endif;
 }

add_action( 'comment_form_before', 'cleanbogger_comment_reply');


/**
 * =============================================
 * Registers an editor stylesheet for the theme.
 * =============================================
 */
function cleanblogger_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'cleanblogger_add_editor_styles' );

/** 
 * ============================================
 * Function for custom post nav
 * ============================================
 */

function cleanblogger_single_post_nav() {

	ob_start();

		$post_nav = get_option('posts_nav');
		if(@$post_nav == 1):
			include get_template_directory() . '/templates/premium-templates/post-nav.php';
		endif;
		
	return ob_get_clean();

}


/** 
 * ============================================
 * Function for sharethis
 * ============================================
 */

function cleanblogger_sharethis() {

	
	ob_start();

		$sharethis = get_option('social_share');
		if(@$sharethis == 1):
			include get_template_directory() . '/templates/premium-templates/social-share.php';
		endif;
		
	return ob_get_clean();
	
}


/** 
 * ============================================
 * Function for author box
 * ============================================
 */

function cleanblogger_author_box() {
	
	ob_start();

		$author_box = get_option('author_box');
		if(@$author_box == 1):
			include get_template_directory() . '/templates/premium-templates/author-box.php';
		endif;
		
	return ob_get_clean();
}


/**
 * ===============================================
 * Functions definitions for custom thumbnail sizes
 * ===============================================
 */

function cleanblogger_cats_related_post() {
	
	ob_start();

		$related_posts = get_option('related_posts');
		if(@$related_posts == 1):
			include get_template_directory() . '/templates/premium-templates/above-footer.php';
		endif;

	return ob_get_clean();
}



/**
 * ===========================================
 * 	Functions definitions for the tooltip
 * ============================================
 */

/** [cleanblogger_success_info description] */
function cleanblogger_tooltip ($atts, $content = null) {

	//get important data and store to variables
	$title = get_the_title();
	$permalink = get_the_permalink();
	$twitterHandler = (get_option('twitter_handler') ? '&amp;via='. esc_attr(get_option('twitter_handler')): '');

	//make the sharelinks work with the collected data
	$facebook   = 'https://www.facebook.com/sharer/sharer.php?u=' .$permalink;
	$twitter    = 'https://twitter.com/intent/tweet?text=' .$content. '&amp;url=' .$permalink. $twitterHandler;
	$comment_link = get_comments_link();


	$atts = shortcode_atts(
		array (
		'title'		=> '',
		'heading'	=> ''
		),
		$atts,
		'tooltip'
	);

		$social_share = '<a href=\''.$twitter.'\' rel=\'nofollow\' target=\'_blank\'> <i class=\'fa fa-twitter fa-lg\' aria-hidden=\'true\'></i></a> &nbsp';

		$social_share .= '<a href=\''.$facebook.'\' rel=\'nofollow\' target=\'_blank\'> <i class=\'fa fa-facebook fa-lg\' aria-hidden=\'true\'></i></a> &nbsp';

		$social_share .= '<a href=\''.$comment_link.'\' rel=\'nofollow\'> | &nbsp comment &nbsp<i class=\'fa fa-comment-o fa-lg\' aria-hidden=\'true\'></i></a>';

		if($atts['title'] == ''): 
			$title = 'Share This';
		else: 
			$title =  $atts['title'];
		endif;

		$output = '<span tabindex="0" class="cleanblogger-popover" data-html=true title="" data-container="body" data-toggle="popover" data-placement="top" 
	    data-content=" <span> Share:</span> '. $social_share .'">';

	    $output .= $content;
	    $output .= '</span>';

	    return $output;
}

add_shortcode("tooltip", "cleanblogger_tooltip");


/**
 * ===========================================
 * 	Functions definitions for contact form
 * ============================================
 */
function cleanblogger_contact_form( $atts ){

        ob_start();

		$contact_form = get_option('contact_form');
		if(@$contact_form == 1):
			include get_template_directory() . '/templates/premium-templates/contact-form.php';
		endif;

	return ob_get_clean();
}
add_shortcode( 'cleanblogger_contact', 'cleanblogger_contact_form' );

/**
 * ===========================================
 * Filter the except length 
 * ============================================
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

/**
 * ============================================
 * Filter the excerpt "read more" string.
 * ============================================
 */
function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

/**
 * ============================================
 * Function for reading time
 * ============================================
 */
function cleanblogger_reading_time() {
	$content = get_post_field('post_content');
	$wordCount = str_word_count(wp_strip_all_tags( $content, true ));
	$reading_time = ceil($wordCount/200);

	if($reading_time == 1):
		$timer = 'Munite Read';
	else: 
		$timer = 'Minutes Read';
	endif;

	$reading_time .= ' ' . $timer;

	return $reading_time; 
}

/**
 * ============================================
 * Function comments nav
 * ============================================
 */

function cleanblogger_comment_nav() {
	if(get_comment_pages_count() > 1 && get_option('page_comments')):

		require(get_template_directory() . '/inc/cleanblogger-comment-nav.php');

	endif;
}

/**
 * ============================================
 * Function comments definitions remove url field
 * ============================================
 */
function cleanblogger_remove_url_field($fields) {
	unset($fields['url']);
	return $fields;
}
add_filter('comment_form_default_fields', 'cleanblogger_remove_url_field');

/**
 * ============================================
 * Function definitions for related post 
 * custom image sizes
 * ============================================
 */
add_image_size( 'cleanblogger_related_post_img', 768, 330, array( 'left', 'top' ) );
add_image_size( 'cleanblogger_header_img', 1400, 520, array( 'left', 'top' ) );
