<h1> Cleanblogger Contact Form</h1>

<?php settings_errors(); ?>

<section class="">
	<div class="row">

		<p>Use this <strong>shortcode</strong> to activate the form inside a page or post</p>
		<code>[cleanblogger_contact]</code>
		
		<form method="post" action="options.php">

			<?php settings_fields( 'cleanblogger-contact-section' ); ?>

			<?php do_settings_sections( 'cleanblogger_contact_form' ); ?>

			<?php submit_button(); ?>

		</form>

	</div>
</section>