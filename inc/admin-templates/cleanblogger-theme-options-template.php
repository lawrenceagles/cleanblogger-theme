<h1> Cleanblogger Theme Options</h1>

<?php settings_errors(); ?>

<section class="">
	<div class="row">
		
		<form method="post" action="options.php">

			<?php settings_fields( 'cleanblogger-theme-support' ); ?>

			<?php do_settings_sections( 'cleanblogger_theme_options' ); ?>

			<?php submit_button(); ?>

		</form>

	</div>
</section>