<?php 
	$picture = esc_attr(get_option('profile_picture'));
	$firstName = esc_attr(get_option('first_name'));
	$lastName = esc_attr(get_option('last_name'));
	$fullName = $firstName . ' ' . $lastName;
	$description = esc_attr(get_option('user_description'));

?>

<h1> cleanblogger Sidebar Options</h1>

<?php settings_errors(); ?>

<section class="">
	<div class="row">

		<div class="cleanblogger-sidebar-wrap">
			<div class="cleanblogger-sidebar-content">
				<div class="container-profile-picture">

					<div id="profile-piture-preview" class="profile-piture-content" style="background-image: url(<?php print $picture; ?>);">
					</div><!-- proifile-picture-content -->

				</div><!-- container-profile-picture -->

				<h1><?php print $fullName; ?></h1>
				<h2><?php print $description ?></h2>

				<div class="cleanblogger-sidebar-social-icons">

				</div>
			</div>
		</div>

		<form method="post" action="options.php" class="cleanblogger-media-form">

			<?php settings_fields( 'cleanblogger-settings-group' ); ?>

			<?php do_settings_sections( 'sidebar_options' ); ?>

			<?php submit_button('Save changes', 'primary', 'btnSubmit'); ?>

		</form>

	</div><!-- row -->
</section><!-- end section -->