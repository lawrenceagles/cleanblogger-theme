<?php
/**
 * ===========================================
 * 	Functions definitions to enhance security
 * ============================================
 */



/**
 * ===========================================
 * 	Functions definitions remove wordpress version
 * 	from assets e.g js and css
 * ============================================
 */
function remove_wp_version_number($src) {
	global $wp_version;

	parse_str(parse_url($src, PHP_URL_QUERY), $query);

	if(!empty($query['ver']) && $query['ver'] === $wp_version):
		$src = remove_query_arg( 'ver', $src );
	endif;

	return $src;
}

add_filter('script_loader_src', 'remove_wp_version_number');

add_filter('style_loader_src', 'remove_wp_version_number');



/**
 * ===========================================
 * 	Functions definitions remove wordpress version
 * 	from meta tags
 * ============================================
 */
function remove_wp_version_meta () {
	return '';
}

add_filter('wp_generator()', 'remove_wp_version_meta');