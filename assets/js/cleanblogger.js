(function($) {
  "use strict"; // Start of use strict

  // init function
  revealPosts();

  // Floating label headings for the contact form
  $("body").on("input propertychange", ".floating-label-form-group", function(e) {
    $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
  }).on("focus", ".floating-label-form-group", function() {
    $(this).addClass("floating-label-form-group-with-focus");
  }).on("blur", ".floating-label-form-group", function() {
    $(this).removeClass("floating-label-form-group-with-focus");
  });

  // Show the navbar when the page is scrolled up
  var MQL = 992;

  //primary navigation slide-in effect
  if ($(window).width() > MQL) {
    var headerHeight = $('#mainNav').height();
    $(window).on('scroll', {
        previousTop: 0
      },
      function() {
        var currentTop = $(window).scrollTop();
        //check if user is scrolling up
        if (currentTop < this.previousTop) {
          //if scrolling up...
          if (currentTop > 0 && $('#mainNav').hasClass('is-fixed')) {
            $('#mainNav').addClass('is-visible');
          } else {
            $('#mainNav').removeClass('is-visible is-fixed');
          }
        } else if (currentTop > this.previousTop) {
          //if scrolling down...
          $('#mainNav').removeClass('is-visible');
          if (currentTop > headerHeight && !$('#mainNav').hasClass('is-fixed')) $('#mainNav').addClass('is-fixed');
        }
        this.previousTop = currentTop;
      });
  }

  
   // File skip-link-focus-fix.js.
   // Helps with accessibility for keyboard only users.
   // Learn more: https://git.io/vWdr2
  
    var isIe = /(trident|msie)/i.test( navigator.userAgent );

    if ( isIe && document.getElementById && window.addEventListener ) {
      window.addEventListener( 'hashchange', function() {
        var id = location.hash.substring( 1 ),
          element;

        if ( ! ( /^[A-z0-9_-]+$/.test( id ) ) ) {
          return;
        }

        element = document.getElementById( id );

        if ( element ) {
          if ( ! ( /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) ) ) {
            element.tabIndex = -1;
          }

          element.focus();
        }
      }, false );
    }

//comment form honeypot codes
 var commentform = $('#comment-form');
 commentform.find('#website').hide();


  /* sidebar functions */
  $(document).on('click', '.js-toggleSidebar', function() {
      $( '.cleanblogger-sidebar' ).toggleClass( 'sidebar-closed' );
      $( 'body' ).toggleClass( 'no-scroll' );
      $( '.sidebar-overlay' ).fadeToggle( 320 );

    });

  // function definition for tooltip for tooltip
  $('[data-toggle="popover"]').popover();


  // ajax load more jquery function
  $(document).on('click', '.cleanblogger-load-more:not(.loading)', function() {

    var that = $(this);
    var page = that.data('page');
    var nextPage = page+1;
    var ajaxUrl = that.data('url');

    that.addClass('loading').find('.load-more-text').slideUp(620);
    that.find('.load-more-icon').addClass('fa-pulse');

    $.ajax({

        url: ajaxUrl,
        type: 'post',
        data: {
          page: page,
          action: 'cleanblogger_load_more'
        },
        error: function(response) {
          console.log(response);
        },
        success: function(response) {

          setTimeout(function() {

            that.data('page', nextPage );
            $('.infinite-loop-posts').append(response);

            
              that.removeClass('loading').find('.load-more-text').slideDown(620);
              that.find('.load-more-icon').removeClass('fa-pulse');

             revealPosts();

          }, 2000);

        }

    });

  });


  // Helper Functions
function revealPosts(){

    var posts = $('article:not(.reveal)');
    var i = 0;

    setInterval(function(){

      if( i >= posts.length ) return false;

      var el = posts[i];

      $(el).addClass('reveal');

      i++

    }, 200);

  }

  /*  contact form submission */
  
  var contactForm = $('#cleanblogger-contact-form');
  contactForm.find("#confirmEmail").hide();
  contactForm.find(".confirmEmail-group").hide();

  contactForm.find("#url").hide();
  contactForm.find(".url-group").hide();



  contactForm.find("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

  contactForm.on('submit', function(e){

        e.preventDefault();

        var form = $(this);
        var name = form.find('#name').val(),
            email = form.find('#email').val(),
            confirmEmail = form.find('#confirmEmail').val(),
            telephone = form.find('#telephone').val(),
            url = form.find('#url').val(),
            phone = form.find('#phone').val(),
            message = form.find('#message').val(),
            ajaxUrl = form.data('url');

        // For Success/Failure Message
        var firstName = name; 

        // Check for white space in name for Success/Fail message
        if (firstName.indexOf(' ') >= 0) {
          firstName = name.split(' ').slice(0, -1).join(' ');
        }

        // Validate the form before submission
        if (name.length <= 0 || email.length <= 0 || phone.length <= 0 || message.length <= 0 ) {  
          return;
        }

        var submitBtn = $("#sendMessageButton");
        submitBtn.html('<i id="cform-icon" class="fa fa-circle-o-notch fa-spin fa-lg fa-fw"></i><span class="sr-only">Sending...</span>Sending...');
        contactForm.find('input, button, textarea').attr('disabled', 'disabled');

        // ajax function for ajax call
        $.ajax({

        url: ajaxUrl,
        type: 'post',
        data: {
          name: name,
          email: email,
          confirmEmail: confirmEmail,
          telephone: telephone,
          phone: phone,
          url: url,
          message: message,
          action: 'cleanblogger_save_contactForm_msg'
        },
        error: function(response) {
          // Fail message
          
          //clear all fields
          $('#cleanblogger-contact-form').trigger("reset");
        },
        success: function(response) {

          if(response != 0) {
            setTimeout(function() {

               // Success message
              $('#success').html("<div class='alert alert-success'>");
              $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
              $('#success > .alert-success').append("<strong>Contratulations your message was successfully sent. </strong>");
              $('#success > .alert-success').append('</div>');                       

            }, 1000);

          }else {
            setTimeout(function() {

              // Failure message
              $('#success').html("<div class='alert alert-danger'>");
              $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
              $('#success > .alert-danger').append("<strong>Message sending failed.<br>Check for errors and tryagain. </strong>");
              $('#success > .alert-danger').append('</div>');                       

            }, 1000);
          }
          //console.log(response);
         // clear all fields
         $('#cleanblogger-contact-form').trigger("reset");
        },
        // disable caching
        cache: false,
        complete: function() {
          setTimeout(function() {
            contactForm.find('input, button, textarea').removeAttr('disabled').val(''); // Re-enable submit button when AJAX call is complete
            submitBtn.html('<i id="cform-icon" class="fa fa-paper-plane-o fa-lg fa-fw"></i><span class="sr-only">Sending...</span>Send');
          }, 1000);
        }

    });

   });

})(jQuery); // End of use strict
