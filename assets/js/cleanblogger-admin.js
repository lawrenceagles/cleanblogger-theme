/*
 * ==================================
 * jQuery script for Media Uploader
 * ==================================
 */

(function($) {
  
  var mediaUploader;

	$('#cleanblogger-profile-picture').on('click', function(e) {
		e.preventDefault();

		if(mediaUploader) {
			mediaUploader.open();
			return;
		}

		mediaUploader = wp.media.frames.file_frame = wp.media({
			title: 'Choose a Profile Picture',
			button: {
				text: 'Choose Picture'
			},
			multiple: false
		});

		mediaUploader.on('select', function (){
			attachment = mediaUploader.state().get('selection').first().toJSON();
			$('#profile_picture').val(attachment.url);

			$('#profile-piture-preview').css('background-image', 'url('+attachment.url+')');
		});

		mediaUploader.open();

	});

	$('#cleanblogger-remove-profile-picture').on('click', function(e) {
		e.preventDefault();

		var userAnswer = confirm("Are you sure you want to remove your profile picture?");
		if(userAnswer == true) {
			$('#profile_picture').val('');
			$('.cleanblogger-media-form').submit();
		}

		return;

	});

})(jQuery);
